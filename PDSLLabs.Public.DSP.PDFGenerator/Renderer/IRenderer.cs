namespace PDSLLabs.Public.DSP.PDFGenerator.Renderer
{
    internal interface IRenderer
    {
        /// <summary>
        /// Renders the entire document according to the current renderer.
        /// </summary>
        /// <returns>The generated PDF document as byte array.</returns>
        byte[] Render();
    }
}
