﻿using iText.IO.Font.Constants;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Font;
using PDSLLabs.Public.DSP.PDFGenerator.DocumentStructures;
using PDSLLabs.Public.DSP.PDFGenerator.DocumentStructures.PickUpReceipt;
using PDSLLabs.DSP.PDFGenerator.Model;
using System.IO;

namespace PDSLLabs.Public.DSP.PDFGenerator.Renderer
{
    /// <summary>
    /// 
    /// </summary>
    public class PickUpReceiptRenderer : IRenderer
    {
        public PickUpReceipt PickUpReceipt { get; }
        protected Document Document { get; set; }
        protected PdfWriter PdfWriter { get; set; }
        protected MemoryStream MemoryStream { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        public PickUpReceiptRenderer(PickUpReceipt pickUpReceipt)
        {
            PickUpReceipt = pickUpReceipt;
            MemoryStream = new MemoryStream();
            PdfWriter = new PdfWriter(MemoryStream);

            // initialize PDF document
            PdfDocument pdfDocument = new PdfDocument(PdfWriter);

            // change the page mode so that the bookmarks panel is opened by default
            pdfDocument.GetCatalog().SetPageMode(PdfName.UseOutlines);

            pdfDocument.InitializeOutlines();
            // create document to add new elements
            Document = new Document(pdfDocument);
            Document.SetMargins(20f, 5f, 20f, 55f);
            FontProvider provider = new FontProvider(StandardFonts.HELVETICA);
            provider.AddStandardPdfFonts();
            provider.AddSystemFonts();

            Document.SetFontProvider(provider);

        }



        public byte[] Render()
        {
            try
            {
                new DocumentStructures.LetterHead(Document, PickUpReceipt.OnlyLogo).Render();
                new Addressfield(Document).Render();
                new MainContent(Document, PickUpReceipt).Render();
                new Footer(Document).Render();
                Document.Close();

                return MemoryStream.ToArray();
            }
            finally
            {
                MemoryStream.Dispose();
            }
        }
    }
}
