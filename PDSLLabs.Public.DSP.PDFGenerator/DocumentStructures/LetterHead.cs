﻿using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.DSP.PDFGenerator.Utilities;
using System;
using static PDSLLabs.Public.DSP.PDFGenerator.Utilities.Conversion;
using static PDSLLabs.Public.DSP.PDFGenerator.Utilities.ElementFactory;
using AddressfieldsModel = PDSLLabs.DSP.PDFGenerator.Model.Addressfields;
using LetterHeadModel = PDSLLabs.DSP.PDFGenerator.Model.LetterHead;

namespace PDSLLabs.Public.DSP.PDFGenerator.DocumentStructures
{
    public class LetterHead : BaseSection
    {
        private bool OnlyLogo { get; set; }
        public LetterHead(Document document, bool onlyLogo) : base(document)
        {
            OnlyLogo = onlyLogo;
        }

        public override void Render()
        {
            if (!OnlyLogo)
            {
                Paragraph letterHead = CreateParagraph(Styles.Fonts.Normal8_5, new Text(LetterHeadModel.DirectorTitle).AddStyle(Styles.Fonts.Bold9))
                    .SetMarginTop(82f)
                    .SetMarginLeft(395f)
                    .SetFixedLeading(12f)
                    .AddTabStops(new TabStop(40, TabAlignment.LEFT))
                    .Add(NewLine)
                    .Add(new Text(LetterHeadModel.DirectorName).AddStyle(Styles.Fonts.Bold9))
                    .Add(NewLine)
                    .Add(new Text(LetterHeadModel.DirectorText))
                    .Add(NewLine)
                    .Add(NewLine);

                Text persInCharge = new Text(LetterHeadModel.PersonInCharge).AddStyle(Styles.Fonts.Bold9);
                letterHead.Add(persInCharge).Add(NewLine).Add(NewLine).Add(NewLine);

                letterHead.Add(new Text(LetterHeadModel.Address[AddressfieldsModel.Street])).Add(NewLine);
                letterHead.Add(new Text(LetterHeadModel.Address[AddressfieldsModel.City])).Add(NewLine);
                letterHead.Add(new Text(LetterHeadModel.Address[AddressfieldsModel.Country])).Add(NewLine).Add(NewLine);

                letterHead.Add(new Text(LetterHeadModel.Building)).Add(NewLine);
                letterHead.Add(new Text(LetterHeadModel.Room)).Add(NewLine).Add(NewLine).Add(NewLine);

                letterHead.Add(new Text("Telefon:")).Add(new Tab()).Add(new Text(LetterHeadModel.Phone)).Add(NewLine);
                letterHead.Add(new Text("Fax:")).Add(new Tab()).Add(new Text(LetterHeadModel.Fax)).Add(NewLine).Add(NewLine);

                letterHead.Add(new Text(LetterHeadModel.Mail)).Add(NewLine).Add(NewLine).Add(NewLine);

                letterHead.Add(new Text(DateTime.Now.ToString("dd.MM.yyyy")).AddStyle(Styles.Fonts.Bold9));

                Document.Add(letterHead);
            }
            //Logo
            Document.Add(new Paragraph());
            PdfDocument pdfDocument = Document.GetPdfDocument();
            ImageData imageData = ImageDataFactory.Create(LetterHeadModel.Logo);
            Image image = new Image(imageData);
            Rectangle pageSize = pdfDocument.GetFirstPage().GetPageSize();
            image.Scale(0.24f, 0.24f);
            Rectangle imageRectangle = new Rectangle(
                image.GetImageScaledWidth() + MillimetersToPoints(5f),
                image.GetImageScaledHeight() + MillimetersToPoints(8f),
                image.GetImageScaledWidth(),
                image.GetImageScaledHeight()
            );

            imageRectangle.SetX(pageSize.GetWidth() - imageRectangle.GetX() + 8);
            imageRectangle.SetY(pageSize.GetHeight() - imageRectangle.GetY() + 12);
            PdfCanvas canvas = new PdfCanvas(pdfDocument.GetFirstPage().GetLastContentStream(), pdfDocument.GetFirstPage().GetResources(), pdfDocument);

            canvas.AddImage(imageData, imageRectangle, true);
            if (!OnlyLogo)
            {
                new PdfCanvas(pdfDocument, 1).SetStrokeColor(Styles.Colors.RwthLightBlue)
                .SetLineWidth(1.1f)
                .MoveTo(390.3, 830)
                .LineTo(390.3, 808.5)
                .MoveTo(390.3, 763.5)
                .LineTo(390.3, 597)
                .ClosePathStroke();
            }
        }
    }
}
