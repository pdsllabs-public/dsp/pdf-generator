﻿using iText.Kernel.Colors;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.DSP.PDFGenerator.Model;
using PDSLLabs.Public.DSP.PDFGenerator.Utilities;
using static PDSLLabs.Public.DSP.PDFGenerator.Utilities.ElementFactory;
using LetterHeadModel = PDSLLabs.DSP.PDFGenerator.Model.LetterHead;

namespace PDSLLabs.Public.DSP.PDFGenerator.DocumentStructures
{
    public class Addressfield : BaseSection
    {
        public Customer Customer { get; set; }
        public Addressfield(Document document, Customer customer) : base(document)
        {
            Customer = customer;
        }
        public Addressfield(Document document) : base(document)
        {

        }
        public override void Render()
        {
            Paragraph addressSender = CreateParagraph(Styles.Fonts.RwthBlue6, $"{LetterHeadModel.Address[Addressfields.Name]} | {LetterHeadModel.Address[Addressfields.Street]} | {LetterHeadModel.Address[Addressfields.City]} | {LetterHeadModel.Address[Addressfields.Country]}")
                .AddTabStops(new TabStop(220f, TabAlignment.LEFT))
                .Add(new Tab())
                .Add(new Text("022000").AddStyle(new Style().SetFontSize(7).SetFontColor(new DeviceRgb(0, 0, 0))))
                .SetMarginTop(-240f)
                .SetMarginLeft(5f);
            Document.Add(addressSender);
            if (Customer != null)
            {
                Paragraph addressReceiver = CreateNormal11Paragraph($"{Customer.Firstname} {Customer.Lastname}")
                    .SetMarginTop(-4f)
                    .SetMarginLeft(5f)
                    .Add(NewLine)
                    .Add(Customer.Address);
                Paragraph here = CreateNormal11Paragraph("-Hier-")
                    .SetMarginTop(15f)
                    .SetMarginLeft(5f);

                Document.Add(addressReceiver)
                .Add(here);
            }

        }
    }
}
