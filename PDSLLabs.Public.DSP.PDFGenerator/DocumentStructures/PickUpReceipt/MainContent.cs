﻿using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.DSP.PDFGenerator.Model;
using PDSLLabs.Public.DSP.PDFGenerator.Utilities;
using static PDSLLabs.Public.DSP.PDFGenerator.Utilities.ElementFactory;
using PickUpReceiptModel = PDSLLabs.DSP.PDFGenerator.Model.PickUpReceipt;

namespace PDSLLabs.Public.DSP.PDFGenerator.DocumentStructures.PickUpReceipt
{
    public class MainContent : BaseSection
    {
        public PickUpReceiptModel PickUpReceipt { get; set; }
        public MainContent(Document document, PickUpReceiptModel pickUpReceipt) : base(document)
        {
            PickUpReceipt = pickUpReceipt;
        }

        public override void Render()
        {
            Paragraph subject = CreateParagraph(Styles.Fonts.Bold11, PickUpReceiptModel.Subject).SetMarginTop(160f);
            Paragraph dateID = CreateParagraph(Styles.Fonts.Bold11, $"{PickUpReceiptModel.OrderIDText} {PickUpReceipt.OrderID}");

            Document.Add(subject);
            Document.Add(dateID);
            RenderOrderTable();

            Paragraph signatureLine = CreateNormal11Paragraph(" ").SetWidth(300).SetBorderBottom(new SolidBorder(1)).SetMarginTop(50);
            Document.Add(signatureLine);
            Paragraph signature = CreateParagraph(Styles.Fonts.Bold9, PickUpReceiptModel.SignatureLineText[0])
                .AddTabStops(new TabStop(50, TabAlignment.LEFT))
                .Add(new Tab())
                .Add(PickUpReceiptModel.SignatureLineText[1])
                .Add(new Tab())
                .Add(new Tab())
                .Add(PickUpReceiptModel.SignatureLineText[2]);
            Document.Add(signature);

            Paragraph note = CreateParagraph(Styles.Fonts.Bold9, PickUpReceiptModel.Note.ToUpper()).SetMarginTop(30f).SetWidth(330);
            Paragraph info = CreateParagraph(Styles.Fonts.Normal9, "");
            foreach (string line in PickUpReceiptModel.Information)
            {
                info.Add(line).Add(NewLine);
            }

            Document.Add(note).Add(info);
        }

        private void RenderOrderTable()
        {
            float[] columnSizes = new float[] { 50, 127, 50, 66, 66, 75, 66 };
            Table orderTable = new Table(columnSizes);
            orderTable.SetMarginTop(30).SetMaxWidth(500);
            int index = 0;
            foreach (string tableHeader in PickUpReceiptModel.ColumnHeaders)
            {
                Cell cell = new Cell().SetMaxWidth(columnSizes[index]);
                cell.Add(new Paragraph(tableHeader).AddStyle(Styles.Fonts.Normal11));
                cell.SetBackgroundColor(Styles.Colors.RwthLightBlue);
                cell.SetBorder(Border.NO_BORDER);
                cell.SetTextAlignment(TextAlignment.CENTER);
                orderTable.AddHeaderCell(cell);
                index++;
            }
            int countEvenOdd = 0;
            foreach (Order order in PickUpReceipt.Orders)
            {
                if (countEvenOdd % 2 == 0)
                {
                    orderTable.AddCell(CreateOrderCell($"{countEvenOdd + 1}", Styles.Colors.RwthGrey10, TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell(order.FileName, Styles.Colors.RwthGrey10, TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell($"{order.Count}", Styles.Colors.RwthGrey10, TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell($"{order.Height}", Styles.Colors.RwthGrey10, TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell($"{order.Width}", Styles.Colors.RwthGrey10, TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell(order.Material, Styles.Colors.RwthGrey10, TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell($"{order.TotalPrice} €", Styles.Colors.RwthGrey10, TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                }
                else
                {
                    orderTable.AddCell(CreateOrderCell($"{countEvenOdd + 1}", TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell(order.FileName, TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell($"{order.Count}", TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell($"{order.Height}", TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell($"{order.Width}", TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell(order.Material, TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                    orderTable.AddCell(CreateOrderCell($"{order.TotalPrice} €", TextAlignment.CENTER).SetMaxWidth(columnSizes[countEvenOdd % columnSizes.Length]));
                }
                countEvenOdd++;
            }

            Document.Add(orderTable);

        }
    }
}
