﻿using iText.Layout;
using iText.Layout.Element;
using PDSLLabs.Public.DSP.PDFGenerator.Utilities;
using static PDSLLabs.Public.DSP.PDFGenerator.Utilities.ElementFactory;
using PickUpReceiptModel = PDSLLabs.DSP.PDFGenerator.Model.PickUpReceipt;

namespace PDSLLabs.Public.DSP.PDFGenerator.DocumentStructures.PickUpReceipt
{
    public class Footer : BaseSection
    {
        public Footer(Document document) : base(document) { }

        public override void Render()
        {
            Paragraph footer = CreateParagraph(Styles.Fonts.Normal9, string.Join("\n", PickUpReceiptModel.Footer)).SetMarginTop(20);
            Document.Add(footer);
        }
    }
}
