﻿using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;

namespace PDSLLabs.Public.DSP.PDFGenerator.DocumentStructures
{
    public abstract class BaseSection
    {
        protected PdfOutline RootOutline { get; }
        protected Document Document { get; set; }
        public static Text NewLine { get; } = new Text("\n");

        public BaseSection(Document document)
        {
            // get root outline from the PdfDocument: false indicates iText does not need to update the outlines.
            RootOutline = document.GetPdfDocument().GetOutlines(false);
            Document = document;
        }

        /// <summary>
        /// Adds page break to document.
        /// </summary>
        public void AddPageBreak()
        {
            Document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
        }

        /// <summary>
        /// Gets default page size. 
        /// </summary>
        /// <returns>The default page size.</returns>
        protected PageSize GetPageSize()
        {
            return Document.GetPdfDocument().GetDefaultPageSize();
        }

        public abstract void Render();

    }
}
