﻿using iText.Kernel.Colors;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;

namespace PDSLLabs.Public.DSP.PDFGenerator.Utilities
{
    public static class ElementFactory
    {

        public static Cell CreateOrderCell(string content, TextAlignment alignment)
        {
            return new Cell().SetBorder(Border.NO_BORDER).Add(CreateNormal11Paragraph(content, alignment));
        }

        public static Cell CreateOrderCell(string content, Color backgroundColor, TextAlignment alignment)
        {
            return CreateOrderCell(content,alignment).SetBackgroundColor(backgroundColor);
        }

        public static Paragraph CreateParagraph(Style style, string content, TextAlignment alignment)
        {
            return CreateParagraph(style, content).SetTextAlignment(alignment);
        }

        public static Paragraph CreateNormal11Paragraph(string content)
        {
            return new Paragraph(content).AddStyle(Styles.Fonts.Normal11);
        }

        public static Paragraph CreateNormal11Paragraph(ILeafElement content)
        {
            return CreateParagraph(Styles.Fonts.Normal11, content);
        }

        public static Paragraph CreateNormal11Paragraph(string content, TextAlignment alignment)
        {
            return CreateNormal11Paragraph(content).SetTextAlignment(alignment);
        }

        public static Paragraph CreateParagraph(Style style, string content)
        {
            return new Paragraph(content).AddStyle(style);
        }

        public static Paragraph CreateParagraph(Style style, ILeafElement content)
        {
            return new Paragraph().Add(content).AddStyle(style);
        }
    }
}
