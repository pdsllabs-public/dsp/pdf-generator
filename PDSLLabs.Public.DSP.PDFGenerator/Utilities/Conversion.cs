﻿namespace PDSLLabs.Public.DSP.PDFGenerator.Utilities
{
    public class Conversion
    {
        // measurement unit in PDF documents; 1 in = 25.4 mm = 72 pt
        // https://kb.itextpdf.com/home/it7kb/faq/how-to-get-the-userunit-from-a-pdf-file

        private const float unitValue = 72f;
        private const float millimitersForInch = 25.4f;

        /// <summary>
        /// Converts value from millimeters to points.
        /// </summary>
        /// <param name="millimiters">Value to convert.</param>
        /// <returns>Converted value in points.</returns>
        public static float MillimetersToPoints(float millimiters)
        {
            return millimiters / millimitersForInch * unitValue;
        }
    }
}
