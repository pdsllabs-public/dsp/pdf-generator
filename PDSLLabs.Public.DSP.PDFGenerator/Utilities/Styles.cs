﻿using iText.IO.Font.Constants;
using iText.Kernel.Colors;
using iText.Layout;

namespace PDSLLabs.Public.DSP.PDFGenerator.Utilities
{
    public static class Styles
    {
        public static class Colors
        {
            public static DeviceRgb RwthBlue => new DeviceRgb(0, 84, 159);
            public static DeviceRgb RwthLightBlue => new DeviceRgb(199, 221, 242);

            public static DeviceRgb RwthGrey10 => new DeviceRgb(236, 237, 237);
            public static DeviceRgb RwthGrey25 => new DeviceRgb(207, 209, 210);
            public static DeviceRgb RwthGrey50 => new DeviceRgb(156, 158, 159);
            public static DeviceRgb Black => new DeviceRgb(0, 0, 0);
        }

        public static class Fonts
        {
            public static Style Normal11 => new Style().SetFontSize(11f);
            public static Style Normal10 => new Style().SetFontSize(10f);
            public static Style Bold11 => Normal11.SetFontFamily(StandardFonts.HELVETICA_BOLD);
            public static Style Normal9 => new Style().SetFontSize(9f);

            public static Style RwthBlue6 = new Style().SetFontSize(6).SetFontColor(Colors.RwthBlue);
            public static Style Bold9 = Normal9.SetFontFamily(StandardFonts.HELVETICA_BOLD);
            public static Style Normal8_5 = new Style().SetFontSize(8.5f);
        }
    }
}
