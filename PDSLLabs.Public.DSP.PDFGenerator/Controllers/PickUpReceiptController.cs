﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDSLLabs.DSP.PDFGenerator.Model;
using PDSLLabs.Public.DSP.PDFGenerator.Renderer;

namespace PDSLLabs.Public.DSP.PDFGenerator
{
    [ApiController]
    [Route("[controller]")]
    public class PickUpReceiptController : ControllerBase
    {
        private ILogger<PickUpReceiptController> Logger { get; }

        public PickUpReceiptController(ILogger<PickUpReceiptController> logger)
        {
            Logger = logger;
        }
        /// <summary>
        /// Say hello!
        /// </summary>
        /// <returns>"Hello"</returns>
        [HttpGet("Hello")]
        public string Get()
        {
            return PickUpReceipt.Subject;
        }

        [HttpPost("RenderPickUpReceipt")]
        [RequestSizeLimit(100000000)]
        public byte[] RenderPickUpReceipt([FromBody] PickUpReceipt pickUpReceipt)
        {
            Logger.Log(LogLevel.Debug, "Start rendering pdf report.");

            IRenderer Renderer = new PickUpReceiptRenderer(pickUpReceipt);
            byte[] file = Renderer.Render();

            Logger.Log(LogLevel.Debug, "Finished rendering pdf report.");
            return file;
        }

    }
}
